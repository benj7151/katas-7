let array1 = ["x", "y", "z"]
let array2 = [1, 2, 3, 4, 5, 6, 7]

function newForEach(array, callback){
    for (let index= 0 ; index < array.length; index++){
        let value = array[index]
        callback(value, index, array)
    }
}

function newMap (array, callback ){
    let value = [];

    for(let index = 0; index <array.length; index++){
        value[index] = callback(array[index], index)
    }
    return value
}

function newFilter (array, callback){
    let value = [];

    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            value.push(array[index])
        }
    }
    return value
}

function newSome (array, callback){
    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            return true
        }
        else{
            return false
        }
    }
}

function newFind(array, callback){
    let value = [];

    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            value.push(array[index])
            break
        }
    }
    return value;
}

function newFindIndex(array, callback){
    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            return index
            break
        }
    }
}

function newEvery(array, callback){
    let isTrue = true
    for(let index = 0; index <array.length; index++){
        if (callback(array[index]) === false){
            isTrue = false
        }
    }
    return isTrue
}
